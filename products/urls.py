from django.urls import path
from products import views

urlpatterns = [
    #Product
    path("list_products/", views.ListProducts.as_view()),
]