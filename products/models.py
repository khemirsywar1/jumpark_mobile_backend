from django.db import models

   
class Product(models.Model):
    name = models.CharField(max_length=500, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    duration = models.FloatField(blank=True, null=True)
    price = models.DecimalField(
        max_digits=10, decimal_places=3, default=0.000)
    image = models.ImageField(upload_to='products/', blank=True, null=True)
    max_person = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f"{self.name}"

